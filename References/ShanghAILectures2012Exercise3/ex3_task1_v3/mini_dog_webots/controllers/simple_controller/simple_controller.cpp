#include <webots/robot.h>
#include <webots/servo.h>

#include <stdio.h>
#include <math.h>


#define PI 3.1416

int main(int argc, char **argv)
{

  int worldBasicTimeStep;
  double current_time_s;
  /* necessary to initialize webots stuff */
  wb_robot_init();
  
  worldBasicTimeStep = wb_robot_get_basic_time_step();

  WbDeviceTag hipDevice1 = wb_robot_get_device("hip0"); //FrontLeft
  WbDeviceTag hipDevice2 = wb_robot_get_device("hip1"); //FrontRight
  WbDeviceTag hipDevice3 = wb_robot_get_device("hip2"); //HindLeft
  WbDeviceTag hipDevice4 = wb_robot_get_device("hip3"); //HindRight
  
  float hipOffset1 = 0.0;
  float hipAmplitude1 = 1.1;
  float hipFrequencyHz1 = 3.0;
  float hipPhaseLag1 = 0.0;
  
  float hipOffset2 = 0.0;
  float hipAmplitude2 = 1.1;
  float hipFrequencyHz2 = 3.0;
  float hipPhaseLag2 = 0.0;
  
  float hipOffset3 = 0.0;
  float hipAmplitude3 = 1.0;
  float hipFrequencyHz3 = 3.0;
  float hipPhaseLag3 = 0.6;
  
  float hipOffset4 = 0.0;
  float hipAmplitude4 = 1.0;
  float hipFrequencyHz4 = 3.0;
  float hipPhaseLag4 = 0.6;
  
  
  /* main loop */
  do {
  
    //printf("The controller is running!\n");
    
    current_time_s = wb_robot_get_time();
    
    float hipCommand1 = hipOffset1 + hipAmplitude1 * sin((2 * PI * hipFrequencyHz1 * current_time_s) - hipPhaseLag1);
    float hipCommand2 = hipOffset2 + hipAmplitude2 * sin((2 * PI * hipFrequencyHz2 * current_time_s) - hipPhaseLag2);
    float hipCommand3 = hipOffset3 + hipAmplitude3 * sin((2 * PI * hipFrequencyHz3 * current_time_s) - hipPhaseLag3);
    float hipCommand4 = hipOffset4 + hipAmplitude4 * sin((2 * PI * hipFrequencyHz4 * current_time_s) - hipPhaseLag4);

    wb_servo_set_position(hipDevice1,hipCommand1);
    wb_servo_set_position(hipDevice2,hipCommand2);
    wb_servo_set_position(hipDevice3,hipCommand3);
    wb_servo_set_position(hipDevice4,hipCommand4);
    
  } while (wb_robot_step(worldBasicTimeStep) != -1);
  
  /* Necessary to cleanup webots stuff */
  wb_robot_cleanup();
  
  return 0;
}
