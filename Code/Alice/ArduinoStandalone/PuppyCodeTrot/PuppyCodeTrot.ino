/* 
 * Beatriz Lopez-Rodriguez, Plymouth University student ID: 10525039
 * Arduino rosserial node for motor control with no sensor feedback.
 * The node outputs force sensitive resistors feedback.
 */

#include <Servo.h>

//Define servo objects
Servo forwardRightservo;
Servo forwardLeftservo;
Servo hindRightservo;
Servo hindLeftservo;

//Signal configuration 

float hipOffset1 = 1.6;
float hipAmplitude1 = 0.6;
float hipFrequencyHz1 = 2.25;
float hipPhaseLag1 = 3.14;

float hipOffset2 = 1.6;
float hipAmplitude2 = 0.6;
float hipFrequencyHz2 = 2.25;
float hipPhaseLag2 = 0.0;

float hipOffset3 = 1.6;
float hipAmplitude3 = 0.5;
float hipFrequencyHz3 = 2.25;
float hipPhaseLag3 = 0.0;

float hipOffset4 = 1.6;
float hipAmplitude4 = 0.5;
float hipFrequencyHz4 = 2.25;
float hipPhaseLag4 = 3.14;

unsigned elapsedTime;

void setup()
{
  forwardRightservo.attach(11);//attach object to pin 
  forwardLeftservo.attach(6);
  hindRightservo.attach(10);
  hindLeftservo.attach(9);
}

void loop()
{
elapsedTime = millis()/100;

    float hipCommand1 = hipOffset1 + hipAmplitude1 * sin(2 * PI * hipFrequencyHz1 * elapsedTime - hipPhaseLag1);
    float hipCommand2 = hipOffset2 + hipAmplitude2 * sin(2 * PI * hipFrequencyHz2 * elapsedTime - hipPhaseLag2);
    float hipCommand3 = hipOffset3 + hipAmplitude3 * sin(2 * PI * hipFrequencyHz3 * elapsedTime - hipPhaseLag3);
    float hipCommand4 = hipOffset4 + hipAmplitude4 * sin(2 * PI * hipFrequencyHz4 * elapsedTime - hipPhaseLag4);

    hipCommand1 = 180 - (hipCommand1*180/PI);
    hipCommand2 = hipCommand2*180/PI;
    hipCommand3 = 180 - (hipCommand3*180/PI);
    hipCommand4 = hipCommand4*180/PI;

    forwardRightservo.write(hipCommand1);
    forwardLeftservo.write(hipCommand2);
    hindRightservo.write(hipCommand3);
    hindLeftservo.write(hipCommand4);

    delay(1);
}
