// Electret Mic BOB12758 knock tutorial extended to two microphones and prepared for inter aural time difference
// knock sound level

const int sampleWindow = 250; // Sample window width in mS (250 mS = 4Hz)
unsigned int knock_right;
unsigned int knock_left;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  unsigned long start = millis(); // Start of sample window
  unsigned int peakToPeak_right = 0;    // peak-to-peak level
  unsigned int peakToPeak_left = 0;    // peak-to-peak level
  unsigned int signalMax_right = 0;
  unsigned int signalMax_left = 0;
  unsigned int signalMin_right = 1024;
  unsigned int signalMin_left = 1024;
  unsigned long timeHeard_right = 0;
  unsigned long timeHeard_left = 0;
  unsigned long interAuralTimeDiference = 0;

  // collect data for 250 miliseconds
  
  while (millis() - start < sampleWindow)
  {
    knock_right = analogRead(A5);
    if (knock_right < 1024)
    {
      if (knock_right > signalMax_right)
      {
        signalMax_right = knock_right; // save just the max levels
      }
      else if (knock_right < signalMin_right)
      {
        signalMin_right = knock_right; // save just the min levels
      }
    }
    knock_left = analogRead(A1);
    if (knock_left < 1024)
    {
      if (knock_left > signalMax_left)
      {
        signalMax_left = knock_left; // save just the max levels
      }
      else if (knock_left < signalMin_left)
      {
        signalMin_left = knock_left; // save just the min levels
      }
    }
  }

  peakToPeak_right = signalMax_right - signalMin_right;  // peak to peak amplitude
  double volts_right = (peakToPeak_right * 3.3)/1024; // convert to volts

  peakToPeak_left = signalMax_left - signalMin_left;  // peak to peak amplitude
  double volts_left = (peakToPeak_left * 3.3)/1024; // convert to volts
  
  Serial.println(volts_right);
     if (volts_right >=1.0)
     {
      Serial.println("Knock Knock Right");
      timeHeard_right = millis();
      Serial.println("timeHeard_right:" timeHeard_right);
     }
  
  Serial.println(volts_left);
     if (volts_left >=1.0)
     {
      Serial.println("Knock Knock Left");
      timeHeard_left = millis();
      Serial.println("timeHeard_left:" timeHeard_left);
     }

interAuralTimeDiference = timeHeard_right - timeHeard_left;
Serial.println(interAuralTimeDiference); 

}



