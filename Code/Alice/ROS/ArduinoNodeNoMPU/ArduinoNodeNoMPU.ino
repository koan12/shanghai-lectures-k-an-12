/* 
 * Beatriz Lopez-Rodriguez, Plymouth University student ID: 10525039
 * Arduino rosserial node for motor control with no sensor feedback.
 * The node outputs force sensitive resistors feedback.
 */

#include <ros.h>
#include <std_msgs/UInt16.h>
#include <Servo.h>

//Declare ros node handle
ros::NodeHandle nh;

//Declare ros publishers to publish touch popics
std_msgs::UInt16 my_msg1, my_msg2, my_msg3, my_msg4;
ros::Publisher my_pub1("Touch1", &my_msg1);
ros::Publisher my_pub2("Touch2", &my_msg2);
ros::Publisher my_pub3("Touch3", &my_msg3);
ros::Publisher my_pub4("Touch4", &my_msg4);

//Declare touch sensors pins variables
int FSR1_Pin;
int FSR2_Pin;
int FSR3_Pin;
int FSR4_Pin;

//Define servo objects
Servo forwardRightservo;
Servo forwardLeftservo;
Servo hindRightservo;
Servo hindLeftservo;

//Define call back functions for receiving servo's position data
void servo_cb1( const std_msgs::UInt16& cmd_msg){
  forwardRightservo.write (180-cmd_msg.data); //set servo angle, should be from 0-180
}
void servo_cb2( const std_msgs::UInt16& cmd_msg){
  forwardLeftservo.write(cmd_msg.data); //set servo angle, should be from 0-180
}
void servo_cb3( const std_msgs::UInt16& cmd_msg){
  hindRightservo.write(180-cmd_msg.data); //set servo angle, should be from 0-180
}
void servo_cb4( const std_msgs::UInt16& cmd_msg){
  hindLeftservo.write(cmd_msg.data); //set servo angle, should be from 0-180
}

//Declare servo's subscription messages
ros::Subscriber<std_msgs::UInt16> my_sub1("forwardrightangle", servo_cb1);
ros::Subscriber<std_msgs::UInt16> my_sub2("forwardleftangle", servo_cb2);
ros::Subscriber<std_msgs::UInt16> my_sub3("hindrightangle", servo_cb3);
ros::Subscriber<std_msgs::UInt16> my_sub4("hindleftangle", servo_cb4);

void setup()
{
  FSR1_Pin = A0;//ForwardLeft
  FSR2_Pin = A1;//ForwardRight
  FSR3_Pin = A2;//HindRight
  FSR4_Pin = A3;//HindLeft
  
  forwardRightservo.attach(11);//attach object to pin 
  forwardLeftservo.attach(6);
  hindRightservo.attach(10);
  hindLeftservo.attach(9);
  
  //Initialize the arduino rosserial node
  nh.initNode();
  
  //Advertise touch sensors analog
  nh.advertise(my_pub1);
  nh.advertise(my_pub2);
  nh.advertise(my_pub3);
  nh.advertise(my_pub4);
  
  //Subscribe to servo controller inputs
  nh.subscribe(my_sub1);
  nh.subscribe(my_sub2);
  nh.subscribe(my_sub3);
  nh.subscribe(my_sub4);
  
}

void loop()
{
  //Read analog output from force sensitive resistors
  int FSR1Reading = analogRead(FSR1_Pin); 
  int FSR2Reading = analogRead(FSR2_Pin);
  int FSR3Reading = analogRead(FSR3_Pin);
  int FSR4Reading = analogRead(FSR4_Pin);
  
  //Publish force sensitive resistors data
  my_msg1.data = FSR1Reading;
  my_pub1.publish(&my_msg1);
  
  my_msg2.data = FSR2Reading;
  my_pub2.publish(&my_msg2);
  
  my_msg3.data = FSR3Reading;
  my_pub3.publish(&my_msg3);
  
  my_msg4.data = FSR4Reading;
  my_pub4.publish(&my_msg4);
  
  //call back messages
  nh.spinOnce();
  delay(1);
}
