#The system is inspired and uses the input control equations proposed by IIDA, F and PFEIFER, R. (2004) in:
#IIDA, F and PFEIFER, R. (2004)
#Cheap Rapid Locomotion of a Quadruped Robot: Self-Stabilization of Bounding Gait. Intelligent Autonomous Systems

import rospy
import math
from std_msgs.msg import UInt16
import time

#initializegait function publishes 4 servo positions which shall be read by the servos in the robot

def initializegait ():
    #declare 4 publishers, one for each servo position
    #declare the initializegait node
    #define publishing rate
    
    pub1 = rospy.Publisher('forwardrightangle', UInt16, queue_size=10)
    pub2 = rospy.Publisher('forwardleftangle', UInt16, queue_size=10)
    pub3 = rospy.Publisher('hindrightangle', UInt16, queue_size=10)
    pub4 = rospy.Publisher('hindleftangle', UInt16, queue_size=10)
    rospy.init_node('initializegait', anonymous=True)
    rate = rospy.Rate(60) # Hz

    #Define variables to create servo positions signal
    
    hipOffset1 = rospy.get_param('~hipOffset1')
    hipAmplitude1 = rospy.get_param('~hipAmplitude1')
    hipFrequencyHz1 = rospy.get_param('~hipFrequencyHz1') 
    hipPhaseLag1 = rospy.get_param('~hipPhaseLag1')

    hipOffset2 = rospy.get_param('~hipOffset2')
    hipAmplitude2 = rospy.get_param('~hipAmplitude2')
    hipFrequencyHz2 = rospy.get_param('~hipFrequencyHz2') 
    hipPhaseLag2 = rospy.get_param('~hipPhaseLag2')

    hipOffset3 = rospy.get_param('~hipOffset3')
    hipAmplitude3 = rospy.get_param('~hipAmplitude3')
    hipFrequencyHz3 = rospy.get_param('~hipFrequencyHz3') 
    hipPhaseLag3 = rospy.get_param('~hipPhaseLag3')

    hipOffset4 = rospy.get_param('~hipOffset4')
    hipAmplitude4 = rospy.get_param('~hipAmplitude4')
    hipFrequencyHz4 = rospy.get_param('~hipFrequencyHz4') 
    hipPhaseLag4 = rospy.get_param('~hipPhaseLag4')

    #Loop function to publish positions

    while not rospy.is_shutdown():
        #Compute positions (angles) in radians 
	seconds = rospy.get_time()
        position1 = hipOffset1 + hipAmplitude1 * math.sin(2*math.pi*hipFrequencyHz1*seconds-hipPhaseLag1)
        position2 = hipOffset2 + hipAmplitude2 * math.sin(2*math.pi*hipFrequencyHz2*seconds-hipPhaseLag2)
        position3 = hipOffset3 + hipAmplitude3 * math.sin(2*math.pi*hipFrequencyHz3*seconds-hipPhaseLag3)
        position4 = hipOffset4 + hipAmplitude4 * math.sin(2*math.pi*hipFrequencyHz4*seconds-hipPhaseLag4)

        #Convert to degrees
	position1 = position1*180.0/math.pi
	position2 = position2*180.0/math.pi
	position3 = position3*180.0/math.pi
	position4 = position4*180.0/math.pi

        #Publish at the rate defined 
        pub1.publish(int(round(position1)))
        pub2.publish(int(round(position2)))
        pub3.publish(int(round(position3)))
        pub4.publish(int(round(position4)))
        rate.sleep()

if __name__ == '__main__':
    try:
        initializegait()
    except rospy.ROSInterruptException:
        pass
