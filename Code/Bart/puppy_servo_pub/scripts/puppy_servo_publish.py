#!/usr/bin/env python

import rospy
from puppy_servo_pub.msg import puppy_servo_msg

def talker():
    pub = rospy.Publisher('chatter', puppy_servo_msg, queue_size=10)
    rospy.init_node('talker', anonymous = True)
    rate = rospy.Rate(10)
    l_speed = 5
    l_control = 1
    l_min_pos = 45
    l_max_pos = 135
    l_offset = 0
    while not rospy.is_shutdown():
        pub.publish(l_speed, l_control, l_min_pos, l_max_pos, l_offset)
        rate.sleep()
        
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
