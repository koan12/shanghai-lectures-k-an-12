/*
*  File: PuppyBart.ino
*  Author: Christopher Wallbridge
*  
*  This file contains all the Arduino code for the 'Puppy' Robot.
*  It is intended to communicate with a server running ROS to
*  allow a user to experiment with different gaits of the robot.
*
*/

//Set the Pins for the Robot
#define trigPin 2 //Define pin for Ultrasonic trigger
#define echoPin 3 //Define pin for Ultrasonic echo
#define flPin 5 //Define pin for front left servo
#define frPin 6 //Define pin for front right servo
#define blPin 8 //Define pin for back left servo
#define brPin 9 //Define pin for back right servo
#define usCycles 50 //Define How many cycles per ultrasonic pulse

//Include from the Arduino Library
#include <Servo.h>

//Include custom made library
#include <PuppyServo.h>

//Include 3rd party Library
//Tim Eckel
//https://bitbucket.org/teckel12/arduino-new-ping/wiki/Home
#include <NewPing.h>

//Include Ros generated libraries
#include <ros.h>
#include <std_msgs/Int64.h>

//Include Ros generated library from custom message
#include <puppy_servo_pub/puppy_servo_msg.h>

// Create variables for ultrasonic
NewPing sonar(trigPin, echoPin, 50);
int cycles;
long distance;

// Create PuppyServo objects
PuppyServo fl;
PuppyServo fr;
PuppyServo bl;
PuppyServo br;

//Control Variables for each servo
int flc = 1;
int frc = 1;
int blc = 1;
int brc = 1;

//Setup Node Handler for Ros
ros::NodeHandle nh;

//Call back functions for subscribers
void flMessageCb( const puppy_servo_pub::puppy_servo_msg& flParam){
  fl.setSpeed(flParam.speed);
  flc = flParam.control;
  fl.setStatus(flc);
  fl.setMinPos(flParam.min_pos);
  fl.setMaxPos(flParam.max_pos);
  fl.setOffset(flParam.offset);
}

void frMessageCb( const puppy_servo_pub::puppy_servo_msg& frParam){
  fr.setSpeed(frParam.speed);
  frc = frParam.control;
  fr.setStatus(frc);
  fr.setMinPos(frParam.min_pos);
  fr.setMaxPos(frParam.max_pos);
  fr.setOffset(frParam.offset);
}

void blMessageCb( const puppy_servo_pub::puppy_servo_msg& blParam){
  bl.setSpeed(blParam.speed);
  blc = blParam.control;
  bl.setStatus(blc);
  bl.setMinPos(blParam.min_pos);
  bl.setMaxPos(blParam.max_pos);
  bl.setOffset(blParam.offset);
}

void brMessageCb( const puppy_servo_pub::puppy_servo_msg& brParam){
  br.setSpeed(brParam.speed);
  brc = brParam.control;
  br.setStatus(brc);
  br.setMinPos(brParam.min_pos);
  br.setMaxPos(brParam.max_pos);
  br.setOffset(brParam.offset);
}

//Setup subscribers to the topics
ros::Subscriber<puppy_servo_pub::puppy_servo_msg> flSub("/flServo", &flMessageCb);
ros::Subscriber<puppy_servo_pub::puppy_servo_msg> frSub("/frServo", &frMessageCb);
ros::Subscriber<puppy_servo_pub::puppy_servo_msg> blSub("/blServo", &blMessageCb);
ros::Subscriber<puppy_servo_pub::puppy_servo_msg> brSub("/brServo", &brMessageCb);

//Setup Publisher for Ultrasonic
std_msgs::Int64 dist_msg;
ros::Publisher distPub("/ultrasonic", &dist_msg);

void setup() {
  Serial.begin(9600);
  
  //Initialise the servos
  fl.init(flPin, 0, 0);
  fr.init(frPin, 0, 0);
  bl.init(blPin, 0, 0);
  br.init(brPin, 0, 0);

  //Set servos to starting positions.
  fl.setPos(90);
  fr.setPos(90);
  bl.setPos(90);
  br.setPos(90);
  
  //Initiate Node Handler
  nh.initNode();
  
  //Subscribe to servo topics
  nh.subscribe(flSub);
  nh.subscribe(frSub);
  nh.subscribe(blSub);
  nh.subscribe(brSub);
  
  //Advertise the ultrasonic topic
  nh.advertise(distPub);
  
  //Setup Ultrasonic
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  
  cycles = usCycles;
}

void loop(){
  
  //Get reading from the ultrasonic sensor based on the cycle
  if (cycles == usCycles){
    distance = sonar.ping_cm();
    dist_msg.data = distance;
    
    distPub.publish( &dist_msg );
    cycles = 0;
  }
  else
  {
    cycles = cycles + 1;
  }
  
  //Handle processing for ROS
  nh.spinOnce();
 
  //Check the distance in cm. Stop moving if an obstacle is too close
  if (distance < 10 && distance != 0){
    stopServos();
  }
  else{
    startServos();
    fl.updatePos();
    fr.updatePos();
    bl.updatePos();
    br.updatePos();
  }
  
  /*Delay necessary to stop pulses from the Ultrasonic from
  interfering with each other, and to allow servos to reach 
  desired position, and to give ROS a chance to process*/
  delay(10);
  
}

void stopServos(){
  //If any of the servos are set to normal motion switch them off
  if ((flc + frc + blc + brc) > 0){
    /*By using this multiplication can switch off the servo while 
    still knowing if it was meant to be moving by use of startServos*/
    flc = flc * -1;
    frc = frc * -1;
    blc = blc * -1;
    brc = brc * -1;
    fl.setStatus(flc);
    fr.setStatus(frc);
    bl.setStatus(blc);
    br.setStatus(brc);
  }
}

void startServos(){
  //If any of the servos have been stopped resume their motion
  if((flc + frc + blc + brc) < 0){
    flc = flc * -1;
    frc = frc * -1;
    blc = blc * -1;
    brc = brc * -1;
    fl.setStatus(flc);
    fr.setStatus(frc);
    bl.setStatus(blc);
    br.setStatus(brc);
  }
}
