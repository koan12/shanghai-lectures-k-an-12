#ifndef _ROS_puppy_servo_pub_puppy_servo_msg_h
#define _ROS_puppy_servo_pub_puppy_servo_msg_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace puppy_servo_pub
{

  class puppy_servo_msg : public ros::Msg
  {
    public:
      int64_t speed;
      int64_t control;
      int64_t min_pos;
      int64_t max_pos;
      int64_t offset;

    puppy_servo_msg():
      speed(0),
      control(0),
      min_pos(0),
      max_pos(0),
      offset(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int64_t real;
        uint64_t base;
      } u_speed;
      u_speed.real = this->speed;
      *(outbuffer + offset + 0) = (u_speed.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_speed.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_speed.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_speed.base >> (8 * 3)) & 0xFF;
      *(outbuffer + offset + 4) = (u_speed.base >> (8 * 4)) & 0xFF;
      *(outbuffer + offset + 5) = (u_speed.base >> (8 * 5)) & 0xFF;
      *(outbuffer + offset + 6) = (u_speed.base >> (8 * 6)) & 0xFF;
      *(outbuffer + offset + 7) = (u_speed.base >> (8 * 7)) & 0xFF;
      offset += sizeof(this->speed);
      union {
        int64_t real;
        uint64_t base;
      } u_control;
      u_control.real = this->control;
      *(outbuffer + offset + 0) = (u_control.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_control.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_control.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_control.base >> (8 * 3)) & 0xFF;
      *(outbuffer + offset + 4) = (u_control.base >> (8 * 4)) & 0xFF;
      *(outbuffer + offset + 5) = (u_control.base >> (8 * 5)) & 0xFF;
      *(outbuffer + offset + 6) = (u_control.base >> (8 * 6)) & 0xFF;
      *(outbuffer + offset + 7) = (u_control.base >> (8 * 7)) & 0xFF;
      offset += sizeof(this->control);
      union {
        int64_t real;
        uint64_t base;
      } u_min_pos;
      u_min_pos.real = this->min_pos;
      *(outbuffer + offset + 0) = (u_min_pos.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_min_pos.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_min_pos.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_min_pos.base >> (8 * 3)) & 0xFF;
      *(outbuffer + offset + 4) = (u_min_pos.base >> (8 * 4)) & 0xFF;
      *(outbuffer + offset + 5) = (u_min_pos.base >> (8 * 5)) & 0xFF;
      *(outbuffer + offset + 6) = (u_min_pos.base >> (8 * 6)) & 0xFF;
      *(outbuffer + offset + 7) = (u_min_pos.base >> (8 * 7)) & 0xFF;
      offset += sizeof(this->min_pos);
      union {
        int64_t real;
        uint64_t base;
      } u_max_pos;
      u_max_pos.real = this->max_pos;
      *(outbuffer + offset + 0) = (u_max_pos.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_max_pos.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_max_pos.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_max_pos.base >> (8 * 3)) & 0xFF;
      *(outbuffer + offset + 4) = (u_max_pos.base >> (8 * 4)) & 0xFF;
      *(outbuffer + offset + 5) = (u_max_pos.base >> (8 * 5)) & 0xFF;
      *(outbuffer + offset + 6) = (u_max_pos.base >> (8 * 6)) & 0xFF;
      *(outbuffer + offset + 7) = (u_max_pos.base >> (8 * 7)) & 0xFF;
      offset += sizeof(this->max_pos);
      union {
        int64_t real;
        uint64_t base;
      } u_offset;
      u_offset.real = this->offset;
      *(outbuffer + offset + 0) = (u_offset.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_offset.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_offset.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_offset.base >> (8 * 3)) & 0xFF;
      *(outbuffer + offset + 4) = (u_offset.base >> (8 * 4)) & 0xFF;
      *(outbuffer + offset + 5) = (u_offset.base >> (8 * 5)) & 0xFF;
      *(outbuffer + offset + 6) = (u_offset.base >> (8 * 6)) & 0xFF;
      *(outbuffer + offset + 7) = (u_offset.base >> (8 * 7)) & 0xFF;
      offset += sizeof(this->offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int64_t real;
        uint64_t base;
      } u_speed;
      u_speed.base = 0;
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 3))) << (8 * 3);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 4))) << (8 * 4);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 5))) << (8 * 5);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 6))) << (8 * 6);
      u_speed.base |= ((uint64_t) (*(inbuffer + offset + 7))) << (8 * 7);
      this->speed = u_speed.real;
      offset += sizeof(this->speed);
      union {
        int64_t real;
        uint64_t base;
      } u_control;
      u_control.base = 0;
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 3))) << (8 * 3);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 4))) << (8 * 4);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 5))) << (8 * 5);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 6))) << (8 * 6);
      u_control.base |= ((uint64_t) (*(inbuffer + offset + 7))) << (8 * 7);
      this->control = u_control.real;
      offset += sizeof(this->control);
      union {
        int64_t real;
        uint64_t base;
      } u_min_pos;
      u_min_pos.base = 0;
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 3))) << (8 * 3);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 4))) << (8 * 4);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 5))) << (8 * 5);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 6))) << (8 * 6);
      u_min_pos.base |= ((uint64_t) (*(inbuffer + offset + 7))) << (8 * 7);
      this->min_pos = u_min_pos.real;
      offset += sizeof(this->min_pos);
      union {
        int64_t real;
        uint64_t base;
      } u_max_pos;
      u_max_pos.base = 0;
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 3))) << (8 * 3);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 4))) << (8 * 4);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 5))) << (8 * 5);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 6))) << (8 * 6);
      u_max_pos.base |= ((uint64_t) (*(inbuffer + offset + 7))) << (8 * 7);
      this->max_pos = u_max_pos.real;
      offset += sizeof(this->max_pos);
      union {
        int64_t real;
        uint64_t base;
      } u_offset;
      u_offset.base = 0;
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 3))) << (8 * 3);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 4))) << (8 * 4);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 5))) << (8 * 5);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 6))) << (8 * 6);
      u_offset.base |= ((uint64_t) (*(inbuffer + offset + 7))) << (8 * 7);
      this->offset = u_offset.real;
      offset += sizeof(this->offset);
     return offset;
    }

    const char * getType(){ return "puppy_servo_pub/puppy_servo_msg"; };
    const char * getMD5(){ return "c298ace812903fa74af3495f2cbd1d1b"; };

  };

}
#endif