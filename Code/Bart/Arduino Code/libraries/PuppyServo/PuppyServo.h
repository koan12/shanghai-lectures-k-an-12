/* 
 * File:   PuppyServo.h
 * Author: Christopher Wallbridge
 *
 * This library is intended to expand upon the Arduino Servo Library to allow 
 * an operator to easily vary the parameters of each servo that control the 
 * gait of the ‘puppy’ robot.
 * 
 * Created on November 25, 2015
 */

//Includes for Standard Arduino Libraries
#include "Arduino.h"
#include "Servo.h"

class PuppyServo
{
public:
    PuppyServo();
    void init(int pin, int offset, int speed);
    void setMinPos(int pos);
    void setMaxPos(int pos);
    void updatePos();
    int getPos();
    void setPos(int pos);
    int getOffset();
    void setOffset(int offset);
    int getSpeed();
    void setSpeed(int speed);
    int getStatus();
    void setStatus(int status);
private:
    int _minPos;
    int _maxPos;
    Servo _myServo;
    int _offset;
    int _speed;
    int _status;
    int _pos;
};
