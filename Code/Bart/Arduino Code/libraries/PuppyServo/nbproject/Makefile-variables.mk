#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-MacOSX
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-MacOSX
CND_ARTIFACT_NAME_Debug=puppyservo
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-MacOSX/puppyservo
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-MacOSX/package
CND_PACKAGE_NAME_Debug=puppyservo.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-MacOSX/package/puppyservo.tar
# Release configuration
CND_PLATFORM_Release=GNU-MacOSX
CND_ARTIFACT_DIR_Release=dist/Release/GNU-MacOSX
CND_ARTIFACT_NAME_Release=puppyservo
CND_ARTIFACT_PATH_Release=dist/Release/GNU-MacOSX/puppyservo
CND_PACKAGE_DIR_Release=dist/Release/GNU-MacOSX/package
CND_PACKAGE_NAME_Release=puppyservo.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-MacOSX/package/puppyservo.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
