/*
 * File:   PuppyServo.cpp
 * Author: Christopher Wallbridge
 *
 * This library is intended to expand upon the Arduino Servo Library to allow 
 * an operator to easily vary the parameters of each servo that control the 
 * gait of the ‘puppy’ robot.
 * 
 * Created on November 25, 2015
 */

//Includes for Standard Arduino Libraries
#include "Arduino.h"
#include "Servo.h"

//Header file
#include "PuppyServo.h"

//Constructor
PuppyServo::PuppyServo()
{
    
}

//Initialise values for the PuppyServo
void PuppyServo::init(int pin, int offset, int speed) {
    _myServo.attach(pin);
    setOffset(offset);
    setSpeed(speed);
    setMinPos(45);
    setMaxPos(135);
    setStatus(1);
}

//Set the minimum position used by updatePos.
void PuppyServo::setMinPos(int pos){
    _minPos = pos;
}

//Set the maximum position used by updatePos.
void PuppyServo::setMaxPos(int pos){
    _maxPos = pos;
}

//Automatically progress through a sweeping motion
void PuppyServo::updatePos(){
    //Find the current position
    int pos = _pos;
    //Check servo should be moving.
    if (_status > 0){
        //Check if there is a delay before the servo should move.
        if (_offset > 0 && _speed > 0){
            _offset = _offset - _speed;
            //Check if the speed has made it go beyond the offset
            if (_offset < 0){
                pos = pos - _offset;
                _offset = 0;
            }
        }
        else if(_offset < 0 && _speed < 0){
            _offset = _offset - _speed;
            //Check if the speed has made it go beyond the offset
            if (_offset > 0){
                pos = pos - _offset;
                _offset = 0;
            }
        }
        else{
            //Update the position of the servo based on the speed.
            pos = pos + _speed;
        }
        //Check if the servo has reached the limit of its sweep.
        if ((pos >= _maxPos && _speed > 0) || (pos <= _minPos && _speed < 0))
        {
            //Reverse direction
            _speed = _speed * -1;
        }
        _pos = pos;
        _myServo.write(pos);
    }
}

//Return current position
int PuppyServo::getPos(){
    return _pos;
}

//Set the servo to a defined position, usually used during setup
void PuppyServo::setPos(int pos){
    _pos = pos;
    _myServo.write(_pos);
}

//Return the offset
int PuppyServo::getOffset(){
    return _offset;
}

//Set the offset
void PuppyServo::setOffset(int offset){
    _offset = offset;
}

//Return the speed
int PuppyServo::getSpeed(){
    return _speed;
}

//Set the speed
void PuppyServo::setSpeed(int speed){
    _speed = speed;
}

//Get the status
int PuppyServo::getStatus(){
    return _status;   
}

//Return the status
void PuppyServo::setStatus(int status){
    _status = status;
}